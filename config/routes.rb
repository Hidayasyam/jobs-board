Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      resources :users 
      resources :jobs
      resources :registrations,:path => 'signup', :as => 'signup'
      resources :sessions,:path => 'login', :as => 'login'
      resources :job_applications, :path => 'application', :as => 'application'
    end
  end
end
