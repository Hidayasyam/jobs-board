class JobPolicy
    attr_reader :user, :job
  
    def initialize(user, job)
      @user = user
      @job = job
    end
  
    def update?
      user.role?
    end

    def destroy?
        user.role?
    end
  
    def create?
    #   byebug
     user.role?
    end
  
end


