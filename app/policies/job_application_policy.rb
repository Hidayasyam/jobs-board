class JobApplicationPolicy
    attr_reader :user, :application
  
    def initialize(user, application)
      @user = user
      @application = application
    end
  
    def update?
      user.role?
    end

    def destroy?
        false
    end

    def create?
    #   byebug
    !user.role?
    end
  
  end