module Api
    module V1
        class UsersController < ApplicationController
          before_action :authorized, only: [:auto_login]
          before_action :set_user, only: [:show]

          def create
            @user = User.create(user_params)
            if @user.valid?
              token = encode_token({user: @user})
              render json: {status: 'success', message: 'Account successfully created', user: @user, token: token}, status: :created
            else
              render json: {status:'error', message: 'Failed to create user account', error: @user.errors.full_messages}
            end
          end

            # LOGGING IN
          def login
            begin
                @user = User.find_!(params[:email]) #Getting dynamic finder to raise exception
                if @user && @user.authenticate(params[:password])
                  token = encode_token({email: @user.email})
                  render json: {status: 'success', message: 'Access granted', user: @user, token: token}, status: :ok
                else
                  render json: {status: 'error', message: "Invalid username or password"}
                end
              end
           end

       def auto_login
          render json: @user
       end

       private

       def set_user
         @user = User.find(params[:email])
       end

      
       def user_params
          params.permit(:email, :password , :password_confirmation)
        end
       end

    end
end
