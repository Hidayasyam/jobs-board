require_relative '../../../services/authenticate_user.rb'

module Api
    module V1
        class JobApplicationsController < ApplicationController
            include AuthenticateUser
            before_action :authorized,only: [:index, :show]
            # if can?
            
            def index #Job Seekers
              if current_user.role
                applications = JobApplication.order('created_at DESC');
                render json: {status: 'SUCCESS', message:'Loaded applications', data:applications},status: :ok
              else 
                applications = JobApplication.where(user_id:current_user.id);
                render json: {status: 'SUCCESS', message:'Loaded applications', data:applications},status: :ok
              end
            end
            
            def show
              application = JobApplication.find(params[:id]) 
              render json: {status: 'SUCCESS', message:'Loaded application', data:application},status: :ok
            end
      
            def create
              application = JobApplication.new(application_params)
              authorize application
              application.update(user_id:current_user.id)
              if application.save
                render json: {status: 'SUCCESS', message:'Saved application', data:application},status: :ok
              else
                render json: {status: 'ERROR', message:'application not saved', data:application.errors},status: :unprocessable_entity
              end
            end
      
            def update
              application = JobApplication.find(params[:id])
              authorize application
              if application.update_column(:status , params[:status] )
                render json: {status: 'SUCCESS', message:'Updated application', data:application},status: :ok
              else
                render json: {status: 'ERROR', message:'application not updated', data:application.errors},status: :unprocessable_entity
              end
            end
      
            private
              # Use callbacks to share common setup or constrcorsaints between actions.
            def set_application
              application = JobApplication.find(params[:id])
            end
            def application_params
              params.permit(:job_id, :status)
            end
          end
    end
end
