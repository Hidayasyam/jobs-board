require_relative '../../../services/authenticate_user.rb'

module Api
    module V1
        class JobsController < ApplicationController
            include AuthenticateUser
            before_action :authorized,only: [:index, :show]
            # if can?
            
            def index #Job Seekers
              jobs = Job.order('created_at DESC');
              render json: {status: 'SUCCESS', message:'Loaded Jobs', data:jobs},status: :ok
            end
            
            def show
              if Job.exists?(params[:id])
                job = Job.find(params[:id])
                render json: {status: 'SUCCESS', message:'Loaded Job', data:job},status: :ok
              else
                render json: {status: :not_found , code: 404, message:'The Job Does Not Exist'}
              end
            end
      
            def create
              job = Job.new(job_params)
              authorize job
              if job.save
                render json: {status: 'SUCCESS', message:'Saved Job', data:job},status: :ok
              else
                render json: {status: 'ERROR', message:'Job not saved', data:job.errors},status: :unprocessable_entity
              end
            end
      
            def destroy
              job = Job.find(params[:id])
              authorize job
              job.destroy
              render json: {status: 'SUCCESS', message:'Deleted Job', data:job},status: :ok
            end
      
            def update
              job = Job.find(params[:id])
              authorize job
              if job.update(job_params)
                render json: {status: 'SUCCESS', message:'Updated Job', data:job},status: :ok
              else
                render json: {status: 'ERROR', message:'Job not updated', data:job.errors},status: :unprocessable_entity
              end
            end
      
            private
              # Use callbacks to share common setup or constrcorsaints between actions.
            def set_job
              job = Job.find(params[:id])
            end
            def job_params
              params.permit(:title, :description)
            end
          end
    end
end