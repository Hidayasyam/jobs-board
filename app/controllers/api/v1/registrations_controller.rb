module Api
    module V1
        class RegistrationsController < ApplicationController

          def create
            @user = User.create(user_params)
            if @user.valid?
              token = AuthenticationTokenServices.encode(@user)
              render json: {status: 'success', message: 'Account successfully created', user: @user, token: token}, status: :created
            else
              render json: {status:'error', message: 'Failed to create user account', error: @user.errors.full_messages}
            end
          end

       private

       def user_params
          params.permit(:email, :password , :password_confirmation)
        end
       end
    end
end
