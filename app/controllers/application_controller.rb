class ApplicationController < ActionController::API
    include Pundit
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    private

    def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore

    render json:{message:"you are not authorized"}
    # redirect_to(request.referrer || root_path)
    end
end
