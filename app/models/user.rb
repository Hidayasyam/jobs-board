class User < ApplicationRecord
  before_validation { self.email = email.downcase }
  has_secure_password
  has_many :applications
  validates :email, presence: true, uniqueness: true
  validates :email, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i ,message: ' Entered is invalid'}
  validates :password, presence: true
end
