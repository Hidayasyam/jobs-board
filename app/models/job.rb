class Job < ApplicationRecord
    has_many :applications
    validates :title, presence: true
  
    validates :description, presence: true
end
