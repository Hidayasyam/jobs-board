class JobApplication < ApplicationRecord
    belongs_to :user
    belongs_to :job
    before_save :check_status

    def check_status
       self.status = false
     end
end
