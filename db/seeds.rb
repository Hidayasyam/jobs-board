# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
  User.create(email: "admin@gmail.com", password: '11223344', password_confirmation:'11223344' ,role:true)
  User.create(email: "jobseeker@gmail.com", password: '12345', password_confirmation:'12345' ,role:false)
  User.create(email: "jobseeker1@gmail.com", password: '12345', password_confirmation:'12345' ,role:false)

