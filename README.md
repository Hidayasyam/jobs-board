

# Description of the Project

The purpose of the exercise is to build a  job-application api using rails framework.  The application must have only one admin that is able only to create a new Job, and update the application status of a job seeker. The Job seeker can view all the Job post and can be create a job applications.  The job seeker registration need a valid email and a password to store it in the database.




# Objectives

- The candidate's email must be unique and saved to database.
- The same email cannot be registered twice, an error message will be displayed to the user indicating that the email already exists.
- Make a simple validation of the user's input 
- The admin is responsible for  Create , Delete and Update the Job Post 


### Job Seekers

- Registrations should be done with email and password (Job Seekers)
- User Registration (Job Seekers only)
- User Login
- The Logged in user only can apply for Job
- The Job Seeker cannot destroy his Job Application
- Current User only can see his own Job Applications
- Only the Logged in User can List all Job Posts


### Admin
- The Admin only can Create a new Job Post 
- The Admin only can Update & Delete existing Job Post
- Only the admin can List all Job Applications
- The Admin can List all Job Posts
## The Admin User :
email : admin@gmail.com

passoword: 11223344


### Job Applications

- You should implement the following functionality:
- Only the Job seekers can apply to any job by creating a Job Application that will have a default status (Not Seen) 
- Only the Admin User can change  the Job Application status (Seen)
- the Admin User cannot change the Job Application Info except the status
- The Admin User cannot apply for any Job

## For the development of the project, the following tools were used:

- Ruby v 2.7.2
- Rails v 7.0.1
- JWT 
- Pundit
- bcrypt
- pg 



## [Deployed Link](https://zenhr-job-api.herokuapp.com/api/v1/)
### **Endpoints of API**

| Method | Endpoint    |        Functionality  |       Permission |
| ------ | ----------- | ------------------- |-------------------: |
| Post    | signup        |         Create new User| Job Seekers |
| Post    | login        |          to login | Job Seekers & Admin|
| GET    | jobs        |          all Job Posts| Job Seekers & Admin|
| GET/:Id   | jobs/2 | specific Job Post |Job Seekers & Admin|
 |  POST   | jobs | Create a new  Job Post |Admin|
|   UPDATE/:Id   | jobs/2 | Update specific Job Post |Admin|
| DELETE/:ID   | jobs/2 | Delete specific Job Post |Admin|
| GET    | application        |   all Job applications    |  Job Seeker`his own applications` & Admin `all applications`|
| GET/:Id   | application/2 | specific Job Application |Job Seeker `his own application if it exists` & Admin `any application`|
|   POST   | application | Create a new  Job Application |Admin|
|   UPDATE/:Id   | application/2 | Update specific Job Application  |Admin|

## How to start the project from your Local environment

- Open your terminal and cd where you want to store the project
- Run the following command - `git clone https://Hidayasyam@bitbucket.org/Hidayasyam/jobs-board.git`
- Cd into the created directory
- Run $ `bundle install`
- On terminal type $ `rails db:create && rails db:migrate && rails db:seed`
- On terminal type $ `rails server`

## Checking the Job system

- Open the browser and type : `http://127.0.0.1:1080/api/v1` on search bar
- Open the landing page at `http://localhost:3000/api/v1`
- Create a new account with a valid email , password and password_confirmation

  ![image](./app/assets/images/signup.png)

## Login In & Apply For Job As a Job Seeker
- Login
 ![image](./app/assets/images/login.png)

- Then set the Bearer Token by taking the token from the login response 
![image](./app/assets/images/setbearer.png)

- Now you can all the Job Posts 
![image](./app/assets/images/alljobs.png)

- Create a new Job Application by passing the `job_id` in the body and set the `Content-Type : application/json`

![image](./app/assets/images/apply.png)

- You can list all your applications and check their status

![image](./app/assets/images/listallapp.png)


## Login In As an Admin
- Login as an Admin

![image](./app/assets/images/loginadmin.png)

- Then set the Bearer Token by taking the token from the login response 
![image](./app/assets/images/adminbearer.png)

- As an Admin you can Create a new Job Post by passing  `title`  & `description` in the body and set the `Content-Type : application/json`
![image](./app/assets/images/postJob.png)

- As an Admin you can Update  by passing the Updating data  `title`  & `description` in the body and set the `Content-Type : application/json`
![image](./app/assets/images/updateJob.png)

- As an Admin you can Delete Job  
![image](./app/assets/images/deleteJob.png)

- As an Admin you can Update Job Application status to `seen` = `true` 

![image](./app/assets/images/updatestatus.png)


[GitHub Account](https://github.com/HidayaSyam)

[Linkedin](https://www.linkedin.com/in/hidayasyam/)

<a href="mailto:HidayaSyam98@gmail.com">Gmail</a>